.. role:: strikethrough
.. role:: red
.. role:: green
.. highlight:: bash

.. _PyPI: https://pypi.org/project/gpaw/
.. _MyQueue: https://myqueue.readthedocs.io/en/latest/
.. _ASE: https://wiki.fysik.dtu.dk/ase/
.. _GPAW: https://wiki.fysik.dtu.dk/gpaw/
.. _reStructuredText: https://docutils.sourceforge.io/rst.html

.. _all slides:

Software
========

I work on these things:

* an electronic structure code (GPAW_)
* a front-end for SLURM/PBS/LSF (MyQueue_)
* the computational materials repository (CMR_)
* the atomic simulation environment project (ASE_)
* the atomic simulation recipes project (ASR_)
* open databases integration for materials design (OPTIMADE_)

.. admonition:: Keywords

   software, HPC, automation

(slides_, source_)

.. _slides: https://jensj.gitlab.io/gpaw-2021-talk/
.. _source: https://gitlab.com/jensj/camd-2021-retreat
.. _OPTIMADE: https://www.optimade.org/
.. _CMR: https://cmr.fysik.dtu.dk/
.. _ASR: https://asr.readthedocs.io/


What I do
=========

* Write code/tests/documentation
* Help others write code
* Get the right people talking together
* Find and fix bugs
* User support
* Maintain infrastructure:

  * GitLab tests (continuous integration or CI)
  * local test-script
  * scripts for building web-pages (ASE, GPAW, CMR)

Get involved:

* Monthly developer meetings (second Monday, 14:00 CEST)
* Code sprints


CMR projects
============

https://cmr.fysik.dtu.dk/ and https://cmrdb.fysik.dtu.dk/

.. image:: pie.svg
    :width: 75 %

``lowdim``: 167767 rows.

Other (1071 rows): ``agau309``, ``organometal``, ``adsorption``, ``abx2``,
``funct_perovskites``, ``absorption_perovskites`` and ``surfaces``.


What's in the CMR Git-repository?
=================================

https://gitlab.com/camd/cmr

* code for running the https://cmrdb.fysik.dtu.dk/ server.
* code and reStructuredText_ files for the https://cmr.fysik.dtu.dk/ web-page
* file layout for each project::

   ├── cmr
   │   ├── proj1
   │   │   ├── custom.py
   │   │   ├── search.html
   │   │   ├── layout.html
   │   │   ├── things.py
   │   │   ├── stuff.py
   │   │   ├── workflow.py
   │   │   ├── test_1.py
   │   │   └── test_2.py
   ├── docs
   │   ├── proj1
   │   │   ├── proj1.rst
   │   │   ├── (proj1.db)
   │   │   ├── example1.py
   │   │   └── example2.py


Future work
===========

* More automatic updates of server software + data
* Add database files to DTU's DOI-service
* Add your project?
* OPTIMADE support
* ...


OPTIMADE
========

From https://www.optimade.org/:

    The Open Databases Integration for Materials Design (OPTIMADE) consortium
    aims to make materials databases inter-operational by developing a common
    REST API.

Example query:

    http://example.org/optimade/v1/structures?filter=nelements=4+AND+chemical_formula="SiO2"

.. admonition:: ASE-backend PR

    https://github.com/Materials-Consortia/optimade-python-tools/pull/856


MyQueue
=======

New features in version 21.8.0:

* Improved ``mq info`` command:

  * ``mq info [folder]``
  * ``mq info -i <id> [folder]``
  * ``mq info --all [folder]``

* *SLURM-lite*: Submit to your own computer::

    $ echo "config = {'scheduler': 'local'}" > .myqueue/config.py
    $ python3 -m myqueue.local > log &  # start server

Future development:

* ASR-integration (simpler and faster cache?)
* ...


GPAW
====

Recent work:

* B-fields (preliminary)
* Documentation improvements
* Raman spectroscopy + electron-phonon coupling updates
* Maximum overlap method (MOM_)
* Direct optimization (LCAO+FD+PW):
  `!900 <https://gitlab.com/gpaw/gpaw/-/merge_requests/900>`__,
  `!864 <https://gitlab.com/gpaw/gpaw/-/merge_requests/864>`__
* Spin-spirals:
  `!912 <https://gitlab.com/gpaw/gpaw/-/merge_requests/912>`__
* PZ-SIC:
  `!864 <https://gitlab.com/gpaw/gpaw/-/merge_requests/864>`__
* MacOS installation instruction:
  `!830 <https://gitlab.com/gpaw/gpaw/-/merge_requests/830>`__

(most of these come from outside our group)

.. Gianluca, Aleksei, Robert, Tuomas, Toma ...

Stalled projects:

* TB-mode
* MGGA clean-up
* optimization of cell-relaxation calculations

... and yes, I'm still working on new PAW data-sets for ground-state and GW
calculations.

.. _MOM: https://wiki.fysik.dtu.dk/gpaw/documentation/mom/mom.html


Investing in the future
=======================

* Cleaning up technical debt:

  * Adopt "functional programming" style with immutable objects
  * New "functions in a unit cell" objects
  * ...

* Make GPAW stretch from laptop to supercomputer

  * PyPI wheels for Linux, Mac, Windows
  * Use PyLibxc
    (`!635 <https://gitlab.com/gpaw/gpaw/-/merge_requests/635>`__)
  * MPI alternative: OpenMP, parallel BLAS, multiprocessing, ...
