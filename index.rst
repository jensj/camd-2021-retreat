=================
CAMd 2021 retreat
=================

Slides
======

.. toctree::
   :glob:

   slides/slide-*

.. toctree::
   :hidden:

   slides/talk

:ref:`All slides in one <all slides>`
