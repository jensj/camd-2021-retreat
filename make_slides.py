from pathlib import Path


def make_slides():
    lines = Path('slides/talk.rst').read_text().splitlines()
    linenumbers = [n - 1
                   for n, line in enumerate(lines)
                   if line.startswith('===')]
    linenumbers.append(len(lines))

    header = '\n'.join(lines[:linenumbers[0] - 2]) + '\n'

    slidenumber = 1
    Path('slides').mkdir(exist_ok=True)
    for n1, n2 in zip(linenumbers[:-1], linenumbers[1:]):
        Path(f'slides/slide-{slidenumber:02}.rst').write_text(
            header + '\n'.join(lines[n1:n2]))
        slidenumber += 1


def setup(app):
    make_slides()
    if not Path('slides/pie.svg').is_file():
        from pies import make_pies
        make_pies()
